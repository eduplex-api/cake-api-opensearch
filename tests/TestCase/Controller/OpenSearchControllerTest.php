<?php
declare(strict_types=1);

namespace OpenSearch\Test\TestCase\Controller;

use App\Test\Fixture\OauthAccessTokensFixture;
use Cake\Http\Client;
use Cake\Http\Exception\InternalErrorException;
use OpenSearch\Controller\OpenSearchController;
use OpenSearch\Lib\Client\OpenSearchBase;
use OpenSearch\Lib\CssMockupService;
use OpenSearch\OpenSearchPlugin;
use RestApi\TestSuite\ApiCommonErrorsTest;
use SbertService\Lib\SbertService;

class OpenSearchControllerTest extends ApiCommonErrorsTest
{
    const TEST_INDEX = 'phpunittest2';

    protected function _getEndpoint(): string
    {
        return OpenSearchPlugin::getRoutePath() . '/openSearch/';
    }

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::_populateOpensearch();
        usleep(800);
    }

    public static function _populateOpensearch(): void
    {
        $httpClient = new Client();
        $body = self::_getBulkBody();
        $options = OpenSearchBase::getOptions();
        $options['type'] = 'json';
        $domain = env('OPEN_SEARCH_DOMAIN');
        if (!$domain) {
            throw new InternalErrorException('OPEN_SEARCH_DOMAIN env is missing');
        }
        $url = $domain  . '/' . self::TEST_INDEX . '/_bulk';
        $httpClient->post($url, $body, $options);
    }

    public function testSearchAllEvents()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
        $expectedTotal = 4;

        $this->get($this->getServiceEndpoint());

        $body = $this->assertJsonResponseOK();
        $total = $body['data']['hits']['total']['value'];
        $this->assertEquals($expectedTotal, $total);
    }

    public function testSearchText()
    {
        $expectedTitle = 'Another event exclusive';

        $query = '&text=' . $expectedTitle;
        $this->get($this->getServiceEndpoint() . $query);

        $body = $this->assertJsonResponseOK();
        $title = $body['data']['hits']['hits'][0]['_source']['title'];
        $this->assertEquals($expectedTitle, $title);

    }

    public function testSearchTextFallbackToWildCard()
    {
        $searchText = 'brand';

        $query = '&text=' . $searchText;
        $this->get($this->getServiceEndpoint() . $query);

        $body = $this->assertJsonResponseOK();
        $title = $body['data']['hits']['hits'][0]['_source']['title'];
        $this->assertTextContains(strtolower($searchText), strtolower($title));

    }

    public function testFilterByTag()
    {
        $expectedTag = 'tag2';

        $query = '&tag=' . $expectedTag;
        $this->get($this->getServiceEndpoint() . $query);

        $body = $this->assertJsonResponseOK();
        $tag = $body['data']['hits']['hits'][0]['_source']['tags'][0];
        $this->assertEquals($expectedTag, $tag['tag']);

    }

    public function testFilterByTagEscoUri()
    {
        $expectedTag = 'tag4';
        $expectedTagEscoUri = 'http://test.esco.uri/2';

        $query = '&tag_esco_uris=' . $expectedTagEscoUri;
        $this->get($this->getServiceEndpoint() . $query);

        $body = $this->assertJsonResponseOK();
        $tag = $body['data']['hits']['hits'][0]['_source']['tags'][0];
        $this->assertEquals($expectedTag, $tag['tag']);
        $this->assertEquals($expectedTagEscoUri, $tag['esco_uri']);

    }

    public function testFilterByUserLevel()
    {
        $userLevel = 20;

        $query = '&user_level=' . $userLevel;
        $this->get($this->getServiceEndpoint() . $query);

        $body = $this->assertJsonResponseOK();
        $hits = $body['data']['hits']['hits'];
        $this->assertEquals(3, count($hits));

    }

    public function testFilterByExclusive()
    {
        $expectedExclusive = '0';
        $expectedCount = 2;
        $expectedBoolean = filter_var($expectedExclusive, FILTER_VALIDATE_BOOLEAN);

        $query = '&has_exclusive_tag=' . $expectedExclusive;
        $this->get($this->getServiceEndpoint() . $query);

        $body = $this->assertJsonResponseOK();
        $services = $body['data']['hits']['hits'];
        $this->assertEquals($expectedCount, count($services));
        foreach ($services as $service) {
            $serviceHasExclusiveTag = $service['_source']['has_exclusive_tag'] ?? false;
            $this->assertEquals($expectedBoolean, $serviceHasExclusiveTag);
        }
    }

    public function testFilterByExclusiveTags()
    {
        $expectedExclusive = '0';
        $expectedExclusiveTag = 'tag2';
        $expectedCount = 3;
        $expectedBoolean = filter_var($expectedExclusive, FILTER_VALIDATE_BOOLEAN);

        $query = '&has_exclusive_tag=' . $expectedExclusive . '&exclusive_tags=' . $expectedExclusiveTag;
        $this->get($this->getServiceEndpoint() . $query);

        $body = $this->assertJsonResponseOK();
        $services = $body['data']['hits']['hits'];
        $this->assertEquals($expectedCount, count($services));
        $correctResults = true;
        foreach ($services as $service) {
            $tag = $service['_source']['tags'][0]['tag'];
            $serviceHasExclusiveTag = $service['_source']['has_exclusive_tag'] ?? false;
            if ($tag !== $expectedExclusiveTag && $expectedBoolean !== $serviceHasExclusiveTag) {
                $correctResults = false;
            }
        }
        $this->assertTrue($correctResults);
    }

    public function testSortByPriceDesc()
    {
        $expectedServiceId = 3;

        $query = '&sort_by=total_price&order=desc';
        $this->get($this->getServiceEndpoint() . $query);

        $body = $this->assertJsonResponseOK();
        $service = $body['data']['hits']['hits'][0]['_source'];
        $this->assertEquals($expectedServiceId, $service['id']);

    }

    public function testSearchWithAlteredScores()
    {
        $expectedFirstElementId = 2;

        $this->get($this->getServiceEndpoint());

        $body = $this->assertJsonResponseOK();
        $firstElement = $body['data']['hits']['hits'][0]['_source'];
        $this->assertEquals($expectedFirstElementId, $firstElement['id']);

    }

    public function testFacetsReturned()
    {
        $expectedFacets = 8;

        $this->get($this->getServiceEndpoint());

        $body = $this->assertJsonResponseOK();
        $facets = $body['data']['aggregations'];
        $this->assertEquals($expectedFacets, count($facets));

    }

    public function testSearchAllEscoSkills()
    {
        $this->get($this->_getEndpoint() . OpenSearchController::ESCO_SKILLS);

        $this->assertJsonResponseOK();
    }

    public function testSearchTextEscoSkills()
    {
        $expectedPrefLabel = 'Wordpress';

        $query = '?plain_text=' . $expectedPrefLabel;
        $this->get($this->_getEndpoint() . OpenSearchController::ESCO_SKILLS . $query);

        $body = $this->assertJsonResponseOK();
        $hit = $body['data']['hits']['hits'][0]['_source'];
        $this->assertTextContains($expectedPrefLabel, $hit['preferredLabel'], '', true);
    }

    public function testSearchSimilarSkillsByText()
    {
        $searchText = 'CSS';

        $this->mockService(SbertService::class, function () {
            return new CssMockupService();
        });

        $query = '?text=' . $searchText;
        $this->get($this->_getEndpoint() . OpenSearchController::ESCO_SKILLS . $query);

        $body = $this->assertJsonResponseOK();
        $hit = $body['data']['hits']['hits'][0]['_source'];
        $this->assertEquals($searchText, $hit['preferredLabel']);
    }

    public function testSearchSimilarSkillsByOccupation()
    {
        $occupations = 'web developer';
        $expectedPrefLabel = 'computer programming';

        $query = '?occupation_labels=' . $occupations;
        $this->get($this->_getEndpoint() . OpenSearchController::ESCO_SKILLS . $query);

        $body = $this->assertJsonResponseOK();
        $hit = $body['data']['hits']['hits'][0]['_source'];
        $this->assertEquals($expectedPrefLabel, $hit['preferredLabel']);
    }

    public function testSearchSimilarSkillsBySkillsLabels()
    {
        $inputSkillsLabels = 'CSS';
        $expectedScore = 2.00;

        $query = '?skills_labels=' . $inputSkillsLabels;
        $this->get($this->_getEndpoint() . OpenSearchController::ESCO_SKILLS . $query);

        $body = $this->assertJsonResponseOK();
        $hit = $body['data']['hits']['hits'][0];
        $this->assertEquals($inputSkillsLabels, $hit['_source']['preferredLabel']);
        $this->assertEquals($expectedScore, round($hit['_score'], 2));
    }

    public function testSearchSimilarSkillsBySkillsUris()
    {
        $inputSkillsUris = 'http://data.europa.eu/esco/skill/e5d1f825-60ed-4bdd-872a-e748c387f777';
        $expectedScore = 2.00;
        $expectedPreferredLabel = 'CSS';

        $query = '?skills_uris=' . $inputSkillsUris;
        $this->get($this->_getEndpoint() . OpenSearchController::ESCO_SKILLS . $query);

        $body = $this->assertJsonResponseOK();
        $hit = $body['data']['hits']['hits'][0];
        $this->assertEquals($inputSkillsUris, $hit['_source']['conceptUri']);
        $this->assertEquals($expectedPreferredLabel, $hit['_source']['preferredLabel']);
        $this->assertEquals($expectedScore, round($hit['_score'], 2));
    }

    public function testSearchTextEscoOccupations()
    {
        $expectedPrefLabel = 'physiotherapist';

        $query = '?plain_text=' . $expectedPrefLabel;
        $this->get($this->_getEndpoint() . OpenSearchController::ESCO_OCCUPATIONS . $query);

        $body = $this->assertJsonResponseOK();
        $hit = $body['data']['hits']['hits'][0]['_source'];
        $this->assertTextContains($expectedPrefLabel, $hit['preferredLabel'], '', true);
    }

    public function testPost_shouldThrowBadRequestExceptionWhenEmptyBodyProvided()
    {
        $this->assertTrue(true);
    }

    public function testPut_shouldThrowBadRequestExceptionWhenNoIdProvided()
    {
        $this->assertTrue(true);
    }

    public function testPut_shouldThrowBadRequestExceptionWhenNoBodyProvided()
    {
        $this->assertTrue(true);
    }

    public function testPatch_shouldThrowBadRequestExceptionWhenNoBodyProvided()
    {
        $this->assertTrue(true);
    }

    public function testPatch_shouldThrowBadRequestExceptionWhenNoIdProvided()
    {
        $this->assertTrue(true);
    }

    public function testDelete_shouldThrowBadRequestExceptionWhenNoIdProvided()
    {
        $this->assertTrue(true);
    }

    private static function _getBulkBody(): string
    {
        $service1 = [
            'id' => 1,
            'title' => 'Event title 1',
            'is_bookable' => true,
            'highlight' => 'Highlight event 1',
            'language' => 'de',
            'total_price' => 147,
            'rating' => 3,
            'has_boost' => false,
            'modules_avg_grade' => 12,
            'provider' => [
                'name' => 'Provider 1',
            ],
            'provider_is_verified' => false,
            'provider_is_topseller' => false,
            'categories' => [
                [
                    'category' => 'category1',
                    'display_name' => 'category 1'
                ],
                [
                    'category' => 'category2',
                    'display_name' => 'category 2'
                ]
            ],
            'tags' => [
                [
                    'tag' => 'tag1',
                    'display_name' => 'tag 1',
                    'grade' => 10
                ],
                [
                    'tag' => 'tag2',
                    'esco_uri' => 'http://esco.uri.test',
                    'display_name' => 'tag 2',
                    'grade' => 10
                ]
            ],
            'trainer_list' => [
                [
                    'profile_pic' => 'https://trainer-pic-test.jpeg',
                    'fullname' => 'Trainer 1'
                ]
            ],
            'next_course_date' => null,
            'rating_rounded' => 300
        ];
        $service2 = [
            'id' => 2,
            'has_exclusive_tag' => true,
            'title' => 'Brandprävention',
            'is_bookable' => true,
            'highlight' => 'Highlight event 2',
            'language' => 'en',
            'modules_avg_grade' => 40,
            'total_price' => 50,
            'rating' => 1,
            'has_boost' => true,
            'provider' => [
                'name' => 'Provider 2',
            ],
            'provider_is_verified' => false,
            'provider_is_topseller' => false,
            'categories' => [
                [
                    'category' => 'category1',
                    'display_name' => 'category 1'
                ]
            ],
            'tags' => [
                [
                    'tag' => 'tag2',
                    'esco_uri' => 'http://esco.uri.test',
                    'display_name' => 'tag 2',
                    'grade' => 20
                ]
            ],
            'trainer_list' => [
                [
                    'profile_pic' => 'https://trainer-pic-test.jpeg',
                    'fullname' => 'Trainer 1'
                ],
                [
                    'profile_pic' => 'https://trainer-pic-test.jpeg',
                    'fullname' => 'Trainer 2'
                ]
            ],
            'next_course_date' => 5000,
            'rating_rounded' => 100
        ];
        $service3 = [
            'id' => 3,
            'has_exclusive_tag' => false,
            'title' => 'Last event',
            'is_bookable' => false,
            'highlight' => 'Highlight event 3',
            'language' => 'de',
            'total_price' => 5000,
            'rating' => null,
            'has_boost' => false,
            'provider' => [
                'name' => 'Provider 3',
            ],
            'provider_is_verified' => true,
            'provider_is_topseller' => true,
            'categories' => [
                [
                    'category' => 'category2',
                    'display_name' => 'category 2'
                ]
            ],
            'tags' => [
                [
                    'tag' => 'tag2',
                    'esco_uri' => 'http://esco.uri.test',
                    'display_name' => 'tag 2',
                    'grade' => null
                ]
            ],
            'next_course_date' => 100,
            'rating_rounded' => 0
        ];
        $service4 = [
            'id' => 4,
            'has_exclusive_tag' => true,
            'title' => 'Another event exclusive',
            'is_bookable' => true,
            'highlight' => 'Highlight event 3',
            'language' => 'en',
            'total_price' => 100,
            'rating' => 0,
            'has_boost' => true,
            'provider' => [
                'name' => 'Provider 3',
            ],
            'provider_is_verified' => false,
            'provider_is_topseller' => false,
            'categories' => [
                [
                    'category' => 'category1',
                    'display_name' => 'category 1'
                ]
            ],
            'tags' => [
                [
                    'tag' => 'tag4',
                    'esco_uri' => 'http://test.esco.uri/2',
                    'display_name' => 'tag 4',
                    'grade' => 10
                ]
            ],
            'next_course_date' => 5000,
            'rating_rounded' => 0
        ];
        $body = '{ "delete": { "_id": "1" } }' . "\n";
        $body .= '{ "delete": { "_id": "2" } }' . "\n";
        $body .= '{ "delete": { "_id": "3" } }' . "\n";
        $body .= '{ "delete": { "_id": "4" } }' . "\n";
        $body .= '{ "index": { "_id": "1" } }' . "\n";
        $body .= json_encode($service1) . "\n";
        $body .= '{ "index": { "_id": "2" } }' . "\n";
        $body .= json_encode($service2) . "\n";
        $body .= '{ "index": { "_id": "3" } }' . "\n";
        $body .= json_encode($service3) . "\n";
        $body .= '{ "index": { "_id": "4" } }' . "\n";
        $body .= json_encode($service4) . "\n";
        return $body;
    }

    public function getServiceEndpoint(): string
    {
        return $this->_getEndpoint() . OpenSearchController::SERVICE . '?tenant=' . self::TEST_INDEX;
    }
}
