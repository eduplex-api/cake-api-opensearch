<?php
declare(strict_types=1);

namespace OpenSearch\Test\TestCase\Controller;

use App\Test\Fixture\AdminUsersFixture;
use App\Test\Fixture\ConfigsFixture;
use App\Test\Fixture\FilesFixture;
use App\Test\Fixture\ModulesFixture;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\OauthClientsFixture;
use App\Test\Fixture\OauthPublicKeysFixture;
use App\Test\Fixture\ServicesFixture;
use App\Test\Fixture\UsersFixture;
use Cake\Http\Client;
use OpenSearch\Controller\OpenSearchModuleFileContentController;
use OpenSearch\OpenSearchPlugin;
use OpenSearch\Test\TestHelpers\TestContentVector;
use RestApi\TestSuite\ApiCommonErrorsTest;
use SbertService\Lib\SbertService;

class OpenSearchModuleFileContentDocumentsControllerTest extends ApiCommonErrorsTest
{
    const TEST_INDEX = 'phpunittest_module_contents_v3';
    const DOCUMENT_ID = 'id01';
    const DOCUMENT_ID_2 = 'id02';
    const DOCUMENT_ID_3 = 'id03';

    protected $fixtures = [
        ConfigsFixture::LOAD, AdminUsersFixture::LOAD, OauthClientsFixture::LOAD,
        OauthPublicKeysFixture::LOAD, OauthAccessTokensFixture::LOAD, UsersFixture::LOAD, ServicesFixture::LOAD,
        ModulesFixture::LOAD, FilesFixture::LOAD
    ];

    protected function _getEndpoint(): string
    {
        return OpenSearchPlugin::getRoutePath() . '/openSearch/moduleFileContent/' . FilesFixture::FILE_ID .
            '/documents/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::_populateOpensearch();
        usleep(711000);
    }

    public function testDeleteDocument()
    {
        $id = self::DOCUMENT_ID;

        $this->delete($this->_getEndpoint() . $id);

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEquals($id, $bodyDecoded['data']['_id']);
        $this->assertEquals('deleted', $bodyDecoded['data']['result']);
    }

    public function testDeleteDocumentWithWrongFileId_throwsException()
    {
        $documentId = 1;

        $this->delete($this->_getEndpoint() . $documentId);

        $this->assertException('DetailedException', 400, 'Document does not exist');
    }

    public function testDeleteDocumentFromAnotherSeller_throwsException()
    {
        $documentId = self::DOCUMENT_ID_2;

        $this->delete($this->_getEndpoint() . $documentId);

        $this->assertException('Forbidden', 403, 'Service does not belong to seller 50');
    }

    public function testEditDocument()
    {
        $id = self::DOCUMENT_ID_3;
        $data = [
            'page' => '5',
            'content' => 'Edited content'
        ];

        $this->mockService(SbertService::class, function () {
            $clientMock = $this->createMock(SbertService::class);
            $clientMock->expects($this->once())->method('vectoriseViaProxy')
                ->willReturn(TestContentVector::TEST_CONTENT_VECTOR);
            return $clientMock;
        });
        $this->patch($this->_getEndpoint() . $id, $data);

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEquals($id, $bodyDecoded['data']['_id']);
        $this->assertEquals('updated', $bodyDecoded['data']['result']);
    }

    public function testEditDocumentWithWrongParams_throwsException()
    {
        $id = self::DOCUMENT_ID_3;
        $data = [
            'test' => 'aasdas'
        ];

        $this->patch($this->_getEndpoint() . $id, $data);

        $this->assertException('Bad Request', 400, 'Invalid body');
    }

    private static function _populateOpensearch(): void
    {
        $httpClient = new Client();
        $options = OpenSearchModuleFileContentController::getOptions();
        $url = env('OPEN_SEARCH_DOMAIN') . '/' . self::TEST_INDEX;
        $httpClient->delete($url, [], $options);
        $options['type'] = 'json';
        $httpClient->put($url, json_encode(OpenSearchModuleFileContentController::getBody()), $options);
        $body = self::_getBulkBody();
        $httpClient->post($url . '/_bulk', $body, $options);
    }

    private static function _getBulkBody(): string
    {
        $doc1 = [
            'serviceId' => ServicesFixture::SERVICE_ID,
            'language' => 'en',
            'moduleId' => ModulesFixture::MODULE_ID,
            'fileId' => FilesFixture::FILE_ID,
            'page' => 1,
            'content' => 'Content of document 1',
            'content_vector' => TestContentVector::TEST_CONTENT_VECTOR
        ];
        $doc2 = [
            'serviceId' => 57,
            'language' => 'en',
            'moduleId' => 1,
            'fileId' => FilesFixture::FILE_ID,
            'page' => 1,
            'content' => 'Content of document 2',
            'content_vector' => TestContentVector::TEST_CONTENT_VECTOR
        ];
        $doc3 = [
            'serviceId' => 2,
            'bookmark' => 'test-bookmark',
            'language' => 'en',
            'moduleId' => 1,
            'fileId' => 2,
            'page' => 3,
            'content' => 'Content of document 3',
            'content_vector' => TestContentVector::TEST_CONTENT_VECTOR
        ];
        $doc4 = [
            'serviceId' => ServicesFixture::SERVICE_ID,
            'language' => 'en',
            'moduleId' => ModulesFixture::MODULE_ID,
            'fileId' => FilesFixture::FILE_ID,
            'page' => 2,
            'content' => 'Content of document 4',
            'content_vector' => TestContentVector::TEST_CONTENT_VECTOR
        ];
        $body = '{ "index": { "_id": "' . self::DOCUMENT_ID . '" } }' . "\n";
        $body .= json_encode($doc1) . "\n";
        $body .= '{ "index": { "_id": "' . self::DOCUMENT_ID_2 . '" } }' . "\n";
        $body .= json_encode($doc2) . "\n";
        $body .= '{ "index": { } }' . "\n";
        $body .= json_encode($doc3) . "\n";
        $body .= '{ "index": { "_id": "' . self::DOCUMENT_ID_3 . '" } }' . "\n";
        $body .= json_encode($doc4) . "\n";
        return $body;
    }
}
