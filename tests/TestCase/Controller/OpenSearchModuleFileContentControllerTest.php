<?php
declare(strict_types=1);

namespace OpenSearch\Test\TestCase\Controller;

use App\Test\Fixture\AdminUsersFixture;
use App\Test\Fixture\ConfigsFixture;
use App\Test\Fixture\FilesFixture;
use App\Test\Fixture\ModulesFixture;
use App\Test\Fixture\OauthAccessTokensFixture;
use App\Test\Fixture\OauthClientsFixture;
use App\Test\Fixture\OauthPublicKeysFixture;
use App\Test\Fixture\ServicesFixture;
use App\Test\Fixture\UsersFixture;
use Cake\Http\Client;
use OpenSearch\Controller\OpenSearchModuleFileContentController;
use OpenSearch\OpenSearchPlugin;
use OpenSearch\Test\TestHelpers\TestContentVector;
use RestApi\TestSuite\ApiCommonErrorsTest;
use SbertService\Lib\SbertService;

class OpenSearchModuleFileContentControllerTest extends ApiCommonErrorsTest
{
    const TEST_INDEX = 'phpunittest_module_contents_v3';
    const TEST_CONTENT = 'Content of module 1 and file 1';

    protected $fixtures = [
        ConfigsFixture::LOAD, AdminUsersFixture::LOAD, OauthClientsFixture::LOAD,
        OauthPublicKeysFixture::LOAD, OauthAccessTokensFixture::LOAD, UsersFixture::LOAD, ServicesFixture::LOAD,
        ModulesFixture::LOAD, FilesFixture::LOAD
    ];

    protected function _getEndpoint(): string
    {
        return OpenSearchPlugin::getRoutePath() . '/openSearch/moduleFileContent/';
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_SELLER);
    }

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::_populateOpensearch();
        usleep(711000);
    }

    private static function _populateOpensearch(): void
    {
        $httpClient = new Client();
        $options = OpenSearchModuleFileContentController::getOptions();
        $url = env('OPEN_SEARCH_DOMAIN') . '/' . self::TEST_INDEX;
        $body = [
            'query' => [
                'match_all' => (object)[]
            ]
        ];
        $res = $httpClient->post($url . '/_delete_by_query', json_encode($body), $options);
        if ($res->getStatusCode() !== 200) {
            debug($res->getStringBody());
        }
        $options['type'] = 'json';
        $httpClient->put($url, json_encode(OpenSearchModuleFileContentController::getBody()), $options);
        $body = self::_getBulkBody();
        $httpClient->post($url . '/_bulk', $body, $options);
    }

    public function testGetData()
    {
        $expectedFileId = FilesFixture::FILE_ID;

        $this->get($this->_getEndpoint() . $expectedFileId);

        $bodyDecoded = $this->assertJsonResponseOK();
        $hits = $bodyDecoded['data']['hits']['hits'];
        $this->assertEquals($expectedFileId, $hits[0]['_source']['fileId']);
    }

    public function testGetList()
    {
        $expectedScore = 2.00;
        $text = self::TEST_CONTENT;

        $this->mockService(SbertService::class, function () {
            $chatClientMock = $this->createMock(SbertService::class);
            $chatClientMock->expects($this->once())->method('vectoriseViaProxy')
                ->willReturn(TestContentVector::TEST_CONTENT_VECTOR);
            return $chatClientMock;
        });

        $this->get($this->_getEndpoint() . '?text=' . $text);

        // if it doesn't throw exception but returns an empty array try increasing the sleep time in setUpBeforeClass
        $bodyDecoded = $this->assertJsonResponseOK();
        $hits = $bodyDecoded['data']['hits']['hits'];
        $this->assertEquals($expectedScore, round($hits[0]['_score'], 2));
    }

    public function testGetList_FilterByFileIdAndPage()
    {

        $this->get($this->_getEndpoint() . '?fileId=2&pageGte=2&pageLte=3');

        $bodyDecoded = $this->assertJsonResponseOK();
        $hits = $bodyDecoded['data']['hits']['hits'];
        $this->assertEquals(1, count($hits));
    }

    public function testGetList_SortByPage()
    {
        $this->get($this->_getEndpoint() . '?sort_by=page&sort_desc=1');

        $bodyDecoded = $this->assertJsonResponseOK();
        $hits = $bodyDecoded['data']['hits']['hits'];
        $this->assertEquals(4, count($hits));
        $this->assertEquals(3, $hits[0]['_source']['page']);
    }

    public function testGetList_FilterByBookmark()
    {
        $bookmark = 'test-bookmark';

        $this->get($this->_getEndpoint() . '?bookmark=' . $bookmark);

        $bodyDecoded = $this->assertJsonResponseOK();
        $hits = $bodyDecoded['data']['hits']['hits'];
        $this->assertEquals(1, count($hits));
        $this->assertEquals($bookmark, $hits[0]['_source']['bookmark']);
    }

    public function testGetDataWithBuyerToken_throwsForbiddenException()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);

        $this->get($this->_getEndpoint() . 1);

        $this->assertException('Forbidden', 403, 'Resource not allowed with this token');
    }

    public function testAddNew()
    {
        $body = [
            'serviceId' => ServicesFixture::SERVICE_ID,
            'language' => 'en',
            'moduleId' => 2,
            'fileId' => 58,
            'page' => 1,
            'content' => 'fake mockup page',
        ];

        $this->mockService(SbertService::class, function () {
            $chatClientMock = $this->createMock(SbertService::class);
            $chatClientMock->expects($this->once())->method('vectoriseViaProxy')
                ->willReturn(TestContentVector::TEST_CONTENT_VECTOR);
            return $chatClientMock;
        });

        $this->post($this->_getEndpoint(), $body);

        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertFalse($bodyDecoded['data']['errors']);
        $this->assertEquals(1, count($bodyDecoded['data']['items']));
    }

    public function testAddNew_throwsExceptionWithMissingParams()
    {
        $body = [
            'serviceId' => ServicesFixture::SERVICE_ID,
            'language' => 'en'
        ];

        $this->post($this->_getEndpoint(), $body);

        $this->assertException('Bad Request', 400, 'Invalid parameters');
    }

    public function testAddNew_throwsExceptionEventNotFound()
    {
        $body = [
            'serviceId' => 2
        ];

        $this->post($this->_getEndpoint(), $body);

        $this->assertException('Not Found', 404, 'Record not found in table "events"');
    }

    public function testAddNew_throwsExceptionEventFromAnotherSeller()
    {
        $body = [
            'serviceId' => 57
        ];

        $this->post($this->_getEndpoint(), $body);

        $this->assertException('Forbidden', 403, 'Service does not belong to seller 50');
    }

    private static function _getBulkBody(): string
    {
        $doc1 = [
            'serviceId' => 1,
            'language' => 'en',
            'moduleId' => 1,
            'fileId' => 1,
            'page' => 1,
            'content' => self::TEST_CONTENT,
            'content_vector' => TestContentVector::TEST_CONTENT_VECTOR
        ];
        $doc2 = [
            'serviceId' => 2,
            'language' => 'en',
            'moduleId' => 1,
            'fileId' => 2,
            'page' => 1,
            'content' => 'Content of document 2',
            'content_vector' => TestContentVector::TEST_CONTENT_VECTOR
        ];
        $doc3 = [
            'serviceId' => 2,
            'bookmark' => 'test-bookmark',
            'language' => 'en',
            'moduleId' => 1,
            'fileId' => 2,
            'page' => 3,
            'content' => 'Content of document 3',
            'content_vector' => TestContentVector::TEST_CONTENT_VECTOR
        ];
        $doc4 = [
            'serviceId' => ServicesFixture::SERVICE_ID,
            'language' => 'en',
            'moduleId' => ModulesFixture::MODULE_ID,
            'fileId' => FilesFixture::FILE_ID,
            'page' => 2,
            'content' => 'Content of document 4',
            'content_vector' => TestContentVector::TEST_CONTENT_VECTOR
        ];
        $body = '{ "index": { } }' . "\n";
        $body .= json_encode($doc1) . "\n";
        $body .= '{ "index": { } }' . "\n";
        $body .= json_encode($doc2) . "\n";
        $body .= '{ "index": { } }' . "\n";
        $body .= json_encode($doc3) . "\n";
        $body .= '{ "index": { } }' . "\n";
        $body .= json_encode($doc4) . "\n";
        return $body;
    }

    public function testDelete_DeleteContent()
    {
        $fileId = FilesFixture::FILE_ID;
        $this->delete($this->_getEndpoint() . $fileId);
        $bodyDecoded = $this->assertJsonResponseOK();
        $this->assertEquals(1, $bodyDecoded['data']['deleted']);
    }

    public function testDeleteFileFromOtherSeller_throwsForbiddenException()
    {
        $fileId = 1;

        $this->delete($this->_getEndpoint() . $fileId);

        $this->assertException('Forbidden', 403, 'File does not belong to seller 50');
    }

    public function testDeleteWithBuyerToken_throwsForbiddenException()
    {
        $this->loadAuthToken(OauthAccessTokensFixture::ACCESS_TOKEN_BUYER);

        $this->delete($this->_getEndpoint() . FilesFixture::FILE_ID);

        $this->assertException('Forbidden', 403, 'Resource not allowed with this token');
    }
}
