<?php

declare(strict_types = 1);

namespace OpenSearch\Test\TestCase\Lib\Client;

use Cake\TestSuite\TestCase;
use OpenSearch\Lib\Client\Client;

class ClientTest extends TestCase
{
    public function testGetSearchDomain()
    {
        $expected = 'https://search-ct2-opensearch-domain-zc5met7fdtvyfxrmrygrddzxmi.eu-central-1.es.amazonaws.com';
        $this->assertEquals($expected, Client::getSearchDomain());
    }
}
