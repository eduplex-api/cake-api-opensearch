<?php

declare(strict_types = 1);

namespace OpenSearch\Controller;

use OpenSearch\OpenSearchPlugin;

class SwaggerJsonController extends \RestApi\Controller\SwaggerJsonController
{
    protected function getContent(\RestApi\Lib\Swagger\SwaggerReader $reader, array $paths): array
    {
        $host = $_SERVER['HTTP_HOST'] ?? 'example.com';
        $plugin = OpenSearchPlugin::getRoutePath();
        $url = 'https://' . $host . $plugin . '/';
        return [
            'openapi' => '3.0.0',
            'info' => [
                'version' => '0.4.0',
                'title' => 'OpenSearch plugin',
                'description' => 'OpenSearch connector plugin',
                'termsOfService' => 'https://www.eduplex.eu/impressum/',
                'contact' => [
                    'name' => 'Eduplex development in Gitlab',
                    'url' => 'https://gitlab.com/eduplex-api/cake-api-opensearch',
                ],
                'license' => [
                    'name' => 'MIT License',
                    'url' => 'https://opensource.org/licenses/MIT',
                ],
            ],
            'servers' => [
                ['url' => $url]
            ],
            'tags' => [],
            'paths' => $paths,
            'components' => [
                'securitySchemes' => [
                    'bearerAuth' => [
                        'type' => 'http',
                        'scheme' => 'bearer',
                    ]
                ],
            ],
        ];
    }
}
