<?php
declare(strict_types=1);

namespace OpenSearch\Controller;

use App\Lib\Helpers\OpenSearchClient;
use Cake\Controller\ComponentRegistry;
use Cake\Event\EventManagerInterface;
use Cake\Http\Client;
use Cake\Http\Client\Exception\NetworkException;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\I18n\FrozenTime;
use Cake\Log\Log;
use RestApi\Lib\Exception\DetailedException;
use SbertService\Lib\SbertService;

/**
 * @property string $OpenSearchUrl
 * @property Client $httpClient
 * @property OpenSearchClient $OpenSearchClient
 *
 */
class OpenSearchModuleFileContentController extends \App\Controller\ApiController
{
    const INDEX_SUFFIX = '_module_contents_v3';

    public function __construct(
        SbertService           $s,
        ?ServerRequest         $request = null,
        ?Response              $response = null,
        ?string                $name = null,
        ?EventManagerInterface $eventManager = null,
        ?ComponentRegistry     $components = null
    ) {
        $this->SbertService = $s;
        parent::__construct($request, $response, $name, $eventManager, $components);
    }

    public function initialize(): void
    {
        parent::initialize();
        $domain = env('OPEN_SEARCH_DOMAIN');
        if (!$domain) {
            throw new InternalErrorException('OPEN_SEARCH_DOMAIN is required');
        }
        $this->OpenSearchUrl = env('OPEN_SEARCH_DOMAIN') . '/' . $this->_getIndexName();
        $this->httpClient = new Client(['timeout' => 30]);
        $this->OpenSearchClient = new OpenSearchClient();
    }

    protected function getData($id)
    {
        $this->OpenSearchClient->validateUser($this->OAuthServer, null, $id);
        $body = $this->_buildGetQuery($id);
        $response = $this->httpClient->post($this->_buildSearchUrl(), $body, self::getOptions());
        $responseDecoded = $response->getJson();
        if ($response->isSuccess()) {
            $data = $responseDecoded;
        } else {
            if ($responseDecoded['error']['type'] === 'index_not_found_exception') {
                $data = $this->_createIndex();
            } else {
                throw new DetailedException($response->getStringBody());
            }
        }
        $this->return = $data;
    }

    public function addNew($data)
    {
        if (($data['type'] ?? '') === OpenSearchClient::MODULES_INDEX_DOCUMENT_SERVICE_SUMMARY) {
            $this->OpenSearchClient->validateUser($this->OAuthServer, $data['id'] ?? null);
            $summaryRes = $this->OpenSearchClient->updateServiceSummaryDocument($data);
            if ($summaryRes->isSuccess()) {
                $ret = $summaryRes->getJson();
            } else {
                throw new DetailedException(
                    'Error adding summary service document to OpenSearch' . $summaryRes->getStringBody()
                );
            }
        } else {
            $this->OpenSearchClient->validateUser(
                $this->OAuthServer,
                $data['serviceId'] ?? null,
                $data['fileId'] ?? null
            );
            $body = $this->_buildPostBody($data);
            $options = self::getOptions();
            $options['type'] = 'json';
            $url = $this->OpenSearchUrl . '/_bulk';
            try {
                $response = $this->httpClient->post($url, $body, $options);
            } catch (NetworkException $e) {
                Log::write('debug', 'NetworkException: ' . $url . ' ' . $body);
                throw $e;
            }
            if ($response->isSuccess()) {
                $ret = $response->getJson();
            } else {
                throw new DetailedException('Error adding document to OpenSearch' . $response->getStringBody());
            }
        }
        $this->return = $ret;
    }

    public function delete($id)
    {
        $this->OpenSearchClient->validateUser($this->OAuthServer, null, $id);
        $response = $this->_deleteDocument($id);
        if ($response->isSuccess()) {
            $data = $response->getJson();
        } else {
            throw new DetailedException('Error deleting documents from OpenSearch' . $response->getStringBody());
        }
        $this->return = $data;
    }

    public function getList()
    {
        $from = $this->request->getQuery('from', 0);
        $size = $this->request->getQuery('size', 5);
        $text = $this->request->getQuery('text', null);
        $type = $this->request->getQuery('type', null);
        $bookmark = $this->request->getQuery('bookmark', null);
        $language = $this->request->getQuery('language', null);
        $sortBy = $this->request->getQuery('sort_by', null);
        $sortDesc = $this->request->getQuery('sort_desc', null);
        $serviceId = intval($this->request->getQuery('serviceId', null));
        $moduleId = intval($this->request->getQuery('moduleId', null));
        $fileId = intval($this->request->getQuery('fileId', null));
        $pageGte = intval($this->request->getQuery('pageGte', null));
        $pageLte = intval($this->request->getQuery('pageLte', null));
        // phpcs:ignore
        $excludedServicesIds = $this->request->getQuery('excludedServicesIds') ? explode(',', $this->request->getQuery('excludedServicesIds')) : [];

        $query = $this->_buildGetListQuery(
            $from,
            $size,
            $text,
            $language,
            $serviceId,
            $moduleId,
            $fileId,
            $excludedServicesIds,
            $type,
            $bookmark,
            $pageGte,
            $pageLte,
            $sortBy,
            $sortDesc
        );

        $response = $this->httpClient->post($this->_buildSearchUrl(), $query, self::getOptions());

        if ($response->isSuccess()) {
            $this->return = $response->getJson();
        } else {
            throw new DetailedException($response->getStringBody());
        }
    }

    private function _buildSearchUrl(): string
    {
        return $this->OpenSearchUrl . '/_search';
    }

    private function _getByFileIdQuery($fileId)
    {
        return json_encode([
            'query' => [
                'bool' => [
                    'filter' => [
                        [
                            'term' => [
                                'fileId' => $fileId
                            ]
                        ]
                    ]
                ]
            ]
        ]);
    }

    private function _buildGetQuery($fileId): string
    {
        return $this->_getByFileIdQuery($fileId);
    }

    private function _buildGetListQuery(
        $from,
        $size,
        $text,
        $language,
        $serviceId,
        $moduleId,
        $fileId,
        $excludedServicesIds,
        $type,
        $bookmark,
        $pageGte,
        $pageLte,
        $sortBy,
        $sortDesc
    ): string {
        $must = [];
        $must_not = [];

        if ($text) {
            $must[] = [
                'knn' => [
                    'content_vector' => [
                        'k' => $size,
                        'vector' => $this->SbertService->vectoriseViaProxy($text, $this->_vectoriseHost())
                    ]
                ]
            ];
        }
        if ($language) {
            $must[] = $this->_buildTermCondition('language', $language);
        }
        if ($serviceId) {
            $must[] = $this->_buildTermCondition('serviceId', $serviceId);
        }
        if ($moduleId) {
            $must[] = $this->_buildTermCondition('moduleId', $moduleId);
        }
        if ($fileId) {
            $must[] = $this->_buildTermCondition('fileId', $fileId);
        }
        if ($type) {
            $must[] = $this->_buildTermCondition('type.keyword', $type);
        }
        if ($bookmark) {
            $must[] = $this->_buildTermCondition('bookmark.keyword', $bookmark);
        }
        if ($pageGte || $pageLte) {
            $range = [
                'range' => [
                    'page' => []
                ]
            ];
            if ($pageGte) {
                $range['range']['page']['gte'] = $pageGte;
            }
            if ($pageLte) {
                $range['range']['page']['lte'] = $pageLte;
            }
            $must[] = $range;
        }

        foreach ($excludedServicesIds as $excludedServiceId) {
            $must_not[] = $this->_buildTermCondition('serviceId', $excludedServiceId);
        }

        $sort = [];
        if ($sortBy) {
            $order = $sortDesc ? 'desc' : 'asc';
            $sort[] = [
                $sortBy => [
                    'order' => $order
                ]
            ];
        }

        return json_encode([
            'from' => $from,
            'size' => $size,
            '_source' => [
                'exclude' => ['content_vector']
            ],
            'query' => [
                'bool' => [
                    'must' => $must,
                    'must_not' => $must_not
                ]
            ],
            'sort' => $sort
        ]);
    }

    private function _buildTermCondition($field, $value): array
    {
        return [
            'term' => [
                $field => [
                    'value' => $value
                ]
            ]
        ];
    }

    private function _vectoriseHost(): string
    {
        // return FullBaseUrl::hostBack();
        return 'https://proto.eduplex.eu';
    }

    private function _buildPostBody($data): string
    {
        if (
            !isset($data['serviceId']) ||
            !isset($data['language']) ||
            !isset($data['moduleId']) ||
            !isset($data['fileId']) ||
            !isset($data['page']) ||
            !isset($data['content'])
        ) {
            throw new BadRequestException('Invalid parameters');
        }
        $doc = [
            'indexedOn' => new FrozenTime(),
            'serviceId' => (int)$data['serviceId'],
            'language' => $data['language'],
            'moduleId' => (int)$data['moduleId'],
            'fileId' => (int)$data['fileId'],
            'page' => (int)$data['page'],
            'heading' => $data['heading'] ?? '',
            'bookmark' => $data['bookmark'] ?? '',
            'content' => $data['content'],
            'content_vector' => $this->SbertService->vectoriseViaProxy($data['content'], $this->_vectoriseHost())
        ];
        if ($data['type'] ?? '') {
            $doc['type'] = $data['type'];
        }
        $body = '{ "index": { } }' . "\n";
        $body .= json_encode($doc) . "\n";
        return $body;
    }

    private function _createIndex(): array
    {
        $responseCreate = $this->httpClient->put(
            $this->OpenSearchUrl, json_encode(self::getBody()), self::getOptions()
        );
        if ($responseCreate->isSuccess()) {
            return $responseCreate->getJson();
        } else {
            throw new DetailedException($responseCreate->getStringBody());
        }
    }

    private function _deleteDocument($fileId): Client\Response
    {
        $deleteUrl = $this->OpenSearchClient->getDeleteUrl();
        $query = $this->OpenSearchClient->buildDeleteQuery($fileId);
        return $this->httpClient->post($deleteUrl, $query, self::getOptions());
    }

    public static function getAuthOpenSearch(): string
    {
        $authLrs = env('AUTH_OPEN_SEARCH');
        return 'Basic ' . $authLrs;
    }

    public static function getBody(): array
    {
        return [
            'settings' => [
                'index' => [
                    'knn' => true
                ]
            ],
            'mappings' => [
                'properties' => [
                    'indexedOn' => [
                        'type' => 'date',
                        'format' => 'strict_date_time_no_millis'
                    ],
                    'serviceId' => [
                        'type' => 'long'
                    ],
                    'moduleId' => [
                        'type' => 'long'
                    ],
                    'fileId' => [
                        'type' => 'long'
                    ],
                    'language' => [
                        'type' => 'text'
                    ],
                    'page' => [
                        'type' => 'long'
                    ],
                    'heading' => [
                        'type' => 'text'
                    ],
                    'bookmark' => [
                        'fields' => [
                            'keyword' => [
                                'type' => 'keyword',
                                'ignore_above' => 256
                            ]
                        ],
                        'type' => 'text'
                    ],
                    'content' => [
                        'type' => 'text'
                    ],
                    'type' => [
                        'fields' => [
                          'keyword' => [
                              'type' => 'keyword',
                              'ignore_above' => 256
                          ]
                        ],
                        'type' => 'text'
                    ],
                    'content_vector' => [
                        'type' => 'knn_vector',
                        'dimension' => 1024,
                        'method' => [
                            'name' => 'hnsw',
                            'engine' => 'faiss',
                            'space_type' => 'innerproduct',
                            'parameters' => [
                                'ef_construction' => 128,
                                'm' => 24
                            ]
                        ]
                    ]
                ]
            ]

        ];
    }

    public static function getOptions(): array
    {
        $headers = [
            'Authorization' => self::getAuthOpenSearch(),
            'Content-Type' => 'application/json',
        ];
        return [
            'headers' => $headers
        ];
    }

    private function _getIndexName(): string
    {
        $env = env('OPEN_SEARCH_INDEX', env('APPLICATION_ENV'));
        if (!$env) {
            throw new InternalErrorException('OPEN_SEARCH_INDEX is not defined');
        }
        return $env . self::INDEX_SUFFIX;
    }
}
