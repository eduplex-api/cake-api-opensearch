<?php
declare(strict_types=1);

namespace OpenSearch\Controller;

use App\Controller\ApiController;
use Cake\Controller\ComponentRegistry;
use Cake\Event\EventManagerInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use OpenSearch\Lib\Client\EscoOccupationsClient;
use OpenSearch\Lib\Client\EscoSkillsClient;
use OpenSearch\Lib\Client\OpenSearchBase;
use OpenSearch\Lib\Client\ServiceClient;
use SbertService\Lib\SbertService;


/**
 * @property SbertService SbertService
 */
class OpenSearchController extends ApiController
{
    const SERVICE = 'service';
    const ESCO_SKILLS = 'esco_skills';
    const ESCO_OCCUPATIONS = 'esco_occupations';

    public function __construct(
        SbertService $s,
        ?ServerRequest $request = null,
        ?Response $response = null,
        ?string $name = null,
        ?EventManagerInterface $eventManager = null,
        ?ComponentRegistry $components = null
    ) {
        $this->SbertService = $s;
        parent::__construct($request, $response, $name, $eventManager, $components);
    }

    public function isPublicController(): bool
    {
        return true;
    }

    private function _getClient(string $id, string $tenant = null): OpenSearchBase
    {
        switch ($id) {
            case self::SERVICE:
                return new ServiceClient($tenant);
            case self::ESCO_SKILLS:
                return new EscoSkillsClient();
            case self::ESCO_OCCUPATIONS:
                return new EscoOccupationsClient();
            default:
                throw new BadRequestException('Invalid search id param');
        }
    }

    protected function getData($id)
    {
        $tenant = $this->request->getQuery('tenant') ?? null;
        $client = $this->_getClient($id, $tenant);
        $data = $client->sendRequest($this->SbertService, $this->request);
        $this->return = $data;
    }
}
