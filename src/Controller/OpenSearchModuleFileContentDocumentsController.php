<?php
declare(strict_types=1);

namespace OpenSearch\Controller;

use RestApi\Lib\Exception\DetailedException;
use App\Lib\Helpers\OpenSearchClient;
use Cake\Controller\ComponentRegistry;
use Cake\Event\EventManagerInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use SbertService\Lib\SbertService;

/**
 * @property OpenSearchClient $OpenSearchClient
 *
 */
class OpenSearchModuleFileContentDocumentsController extends \App\Controller\ApiController
{
    public function __construct(
        SbertService           $s,
        ?ServerRequest         $request = null,
        ?Response              $response = null,
        ?string                $name = null,
        ?EventManagerInterface $eventManager = null,
        ?ComponentRegistry     $components = null
    ) {
        $this->SbertService = $s;
        parent::__construct($request, $response, $name, $eventManager, $components);
    }

    public function initialize(): void
    {
        parent::initialize();
        $this->OpenSearchClient = new OpenSearchClient();
    }

    public function delete($id)
    {
        $this->_validate($id);
        $response = $this->OpenSearchClient->deleteDocumentById($id);
        if ($response->isSuccess()) {
            $this->return = $response->getJson();
        } else {
            throw new DetailedException('Error deleting document from Opensearch' . $response->getStringBody());
        }
    }

    public function edit($id, $data)
    {
        $this->_validate($id);
        $page = $data['page'] ?? null;
        $content = $data['content'] ?? null;
        if (!$page && !$content) {
            throw new BadRequestException('Invalid body');
        }
        $payload = [];
        if ($page) {
            $payload['page'] = (int)$page;
        }
        if ($content) {
            $vectorizeHost = 'https://proto.eduplex.eu';
            $payload['content'] = $content;
            $payload['content_vector'] = $this->SbertService->vectoriseViaProxy($content, $vectorizeHost);
        }
        $response = $this->OpenSearchClient->editDocumentById($id, $payload);
        if ($response->isSuccess()) {
            $this->return = $response->getJson();
        } else {
            throw new DetailedException('Error editing document from Opensearch' . $response->getStringBody());
        }

    }

    private function _getDocument($id)
    {
        $docRes = $this->OpenSearchClient->getDocumentById($id);
        if ($docRes->isSuccess()) {
            return $docRes->getJson();
        } else {
            throw new DetailedException('Document does not exist');
        }
    }

    private function _validate($id)
    {
        $doc = $this->_getDocument($id);
        $serviceId = $doc['_source']['serviceId'] ?? null;
        $fileId = $doc['_source']['fileId'] ?? null;
        if ($fileId != $this->request->getParam('fileID')) {
            throw new ForbiddenException('Document does not belong to this fileId');
        }
        $this->OpenSearchClient->validateUser($this->OAuthServer, $serviceId, $fileId);
    }
}
