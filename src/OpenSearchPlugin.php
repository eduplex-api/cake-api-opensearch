<?php
declare(strict_types=1);

namespace OpenSearch;

use Cake\Core\ContainerInterface;
use Cake\Http\ServerRequest;
use Cake\Routing\RouteBuilder;
use OpenSearch\Controller\OpenSearchController;
use OpenSearch\Controller\OpenSearchModuleFileContentController;
use OpenSearch\Controller\OpenSearchModuleFileContentDocumentsController;
use RestApi\Lib\RestPlugin;
use SbertService\Lib\SbertService;

class OpenSearchPlugin extends RestPlugin
{
    protected function routeConnectors(RouteBuilder $builder): void
    {
        $builder->connect('/openSearch/moduleFileContent/{fileID}/documents/*',
            OpenSearchModuleFileContentDocumentsController::route()
        );
        $builder->connect('/openSearch/moduleFileContent' . '/*', OpenSearchModuleFileContentController::route());
        $builder->connect('/openSearch' . '/*', OpenSearchController::route());
        $builder->connect('/OpenSearch/openapi/*', \OpenSearch\Controller\SwaggerJsonController::route());
    }

    public function services(ContainerInterface $container): void
    {
         $container->addShared(SbertService::class);// addShared means singleton
         $container->add(OpenSearchController::class)
             ->addArguments([SbertService::class, ServerRequest::class]);
        $container->add(OpenSearchModuleFileContentController::class)
            ->addArguments([SbertService::class, ServerRequest::class]);
        $container->add(OpenSearchModuleFileContentDocumentsController::class)
            ->addArguments([SbertService::class, ServerRequest::class]);
    }
}
