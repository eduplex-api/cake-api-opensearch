<?php
declare(strict_types=1);

namespace OpenSearch\Lib;

use Cake\Http\ServerRequest;
use SbertService\Lib\SbertService;

abstract class QueryBuilderBase
{
    private ServerRequest $_request;
    protected SbertService $_sbertService;

    public function __construct(SbertService $s, ServerRequest $request)
    {
        $this->_request = $request;
        $this->_sbertService = $s;
    }

    protected function _getLanguage(): string
    {
        $language = $this->_request->getHeader('Accept-Language');
        if ($language && isset($language[0]) && $language[0] && strlen($language[0]) === 2) {
            return $language[0];
        }
        return 'en';
    }

    protected function _getQueryParams(): array
    {
        return $this->_request->getQueryParams();
    }

    protected function _buildFallbackQuery(): ?array
    {
        return null;
    }
}
