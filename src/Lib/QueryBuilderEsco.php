<?php
declare(strict_types=1);

namespace OpenSearch\Lib;

use App\Lib\FullBaseUrl;
use Cake\Http\ServerRequest;
use OpenSearch\Lib\Client\EscoSkillsClient;
use OpenSearch\Model\Table\TagsTable;
use SbertService\Lib\SbertService;

/**
 * @property TagsTable $Tags
 */
class QueryBuilderEsco extends QueryBuilderBase
{
    public function __construct(SbertService $s, ServerRequest $request)
    {
        parent::__construct($s, $request);
        $this->Tags = TagsTable::load();
    }

    public function buildQuery(): array
    {
        $limit = $this->_getQueryParams()['limit'] ?? null;
        $page = $this->_getQueryParams()['page'] ?? null;
        $text = $this->_getQueryParams()['text'] ?? '';
        $size = $limit ?? 10;
        $from = $page ? $size * ($page - 1) : 0;
        $vectors = [];
        if (isset($this->_getQueryParams()['occupation_labels'])) {
            //User->Edu API:Search for skills based on **occupation**
            //Edu API->OpenSearch: request to get skills of the **occupation**
            //Edu API<-OpenSearch:return **skills**
            //Edu API->OpenSearch:performs semantic search(knn) with skills **vector_description**
            //Edu API<-OpenSearch:return **skills**
            //User<-Edu API:return **skills**
            $labels = explode(',', $this->_getQueryParams()['occupation_labels']);
            $hits = $this->_getSkillsByOccupationLabels($labels);
            $vectors = $this->_addSkillsVectors($hits, $vectors);
        }
        if (isset($this->_getQueryParams()['skills_labels'])) {
            //User->Edu API:Search for similar skills to the skill with preferredLabel **css**
            //Edu API->OpenSearch:look for **css** vector based on the preferredLabel
            //Edu API<-OpenSearch:returns vector
            //Edu API->OpenSearch:performs semantic search with **css** vector_description
            //Edu API<-OpenSearch:returns similar skills
            //User<-Edu API:returns similar skills
            $labels = explode(',', $this->_getQueryParams()['skills_labels']);
            $hits = $this->_getSkillsBySkillsLabels($labels);
            $vectors = $this->_addSkillsVectors($hits, $vectors);
        }
        if (isset($this->_getQueryParams()['skills_uris'])) {
            //User->Edu API:Search for similar skills to the skill **css**
            //with conceptUri **http://data.europa.eu/esco/skill/e5d1f825-60ed-4bdd-872a-e748c387f777**
            //Edu API->OpenSearch:look for **css** vector based on the uri
            //Edu API<-OpenSearch:returns vector
            //Edu API->OpenSearch:performs semantic search with **css** vector_description
            //Edu API<-OpenSearch:returns similar skills
            //User<-Edu API:returns similar skills
            $labels = explode(',', $this->_getQueryParams()['skills_uris']);
            $hits = $this->_getSkillsBySkillsUris($labels);
            $vectors = $this->_addSkillsVectors($hits, $vectors);
        }
        if ($text) {
            //User->Edu API:Search terms (CSS)
            //Edu API->Sbert:vectorize text
            //Sbert->Edu API: returns vector
            //Edu API->OpenSearch:performs semantic search(knn) with vector
            //
            //Edu API<-OpenSearch:returns similar skills
            //User<-Edu API:returns similar skills
            $domain = FullBaseUrl::host();
            $vectors[] = [
                'script_score' => [
                    'query' => [
                        'match_all' => (object)[]
                    ],
                    'script' => [
                        'source' => 'knn_score',
                        'lang' => 'knn',
                        'params' => [
                            'field' => 'vector_description',
                            'query_value' => $this->_sbertService->vectoriseViaProxy($text, $domain),
                            'space_type' => 'innerproduct'
                        ]
                    ]
                ]
            ];
            $vectors[] = [
                'script_score' => [
                    'query' => [
                        'match_all' => (object)[]
                    ],
                    'script' => [
                        'source' => 'knn_score',
                        'lang' => 'knn',
                        'params' => [
                            'field' => 'vector_labels',
                            'query_value' => $this->_sbertService->vectoriseViaProxy($text, $domain),
                            'space_type' => 'innerproduct'
                        ]
                    ]
                ]
            ];
        }
        if (count($vectors) > 0) {
            $searchQuery = [
                'bool' => [
                    'should' => $vectors
                ]
            ];
        } else {
            $plainText = $this->_getQueryParams()['plain_text'] ?? '';
            $searchQuery = [
                'multi_match' => [
                    'query' => $plainText,
                    'fields' => [
                        'preferredLabel^6',
                        'altLabels^2',
                        'description',
                        'conceptUri'
                    ],
                    'fuzziness' => 'AUTO',
                    'zero_terms_query' => 'all'
                ]
            ];
        }
        $query = [
            'from' => $from,
            'size' => $size,
            '_source' => [
                'exclude' => ['vector_*']
            ],
            'query' => [
                'bool' => [
                    'must' => $searchQuery
                ]
            ]
        ];
        $query['query']['bool']['filter'] = $this->_getFilters(true);
        return $query;
    }

    private function _getFilters($filterByPlatform = false): array
    {
        $language = $this->_getLanguage();
        $filters = [
            [
                'term' => [
                    'langCode.keyword' => $language
                ]
            ]
        ];
        if (isset($this->_getQueryParams()['uris'])) {
            $uris = explode(',', $this->_getQueryParams()['uris']);
            $filters[] = [
                'terms' => [
                    'conceptUri.keyword' => $uris
                ]
            ];
        } else if (
            isset($this->_getQueryParams()['only_platform_skills']) &&
            $this->_getQueryParams()['only_platform_skills'] &&
            $filterByPlatform
        ) {
            $tagsUris = $this->Tags->getAllUrisForSearchFilter();
            $filters[] = [
                'terms' => [
                    'conceptUri.keyword' => $tagsUris
                ]
            ];
        }
        return $filters;
    }

    private function _getSkillsByOccupationLabels(array $labels): array
    {
        $occupationsQuery = [
            'size' => 300,
            'query' => [
                'bool' => [
                    'filter' => $this->_getFilters()
                ]
            ]
        ];
        $occupationsQuery['query']['bool']['filter'][] = [
            'nested' => [
                'path' => 'relatedOccupations',
                'query' => [
                    'bool' => [
                        'must' => [
                            [
                                'terms' => [
                                    'relatedOccupations.occupationPreferredLabel.keyword' => $labels
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        return $this->_getSkills($occupationsQuery);
    }

    private function _getSkillsBySkillsLabels(array $labels): array
    {
        $skillsQuery = [
            'size' => 300,
            'query' => [
                'bool' => [
                    'filter' => $this->_getFilters()
                ]
            ]
        ];
        $skillsQuery['query']['bool']['filter'][] = [
            'terms' => [
                'preferredLabel.keyword' => $labels
            ]
        ];
        return $this->_getSkills($skillsQuery);
    }

    private function _getSkillsBySkillsUris(array $uris): array
    {
        $skillsQuery = [
            'size' => 300,
            'query' => [
                'bool' => [
                    'filter' => $this->_getFilters()
                ]
            ]
        ];
        $skillsQuery['query']['bool']['filter'][] = [
            'terms' => [
                'conceptUri.keyword' => $uris
            ]
        ];
        return $this->_getSkills($skillsQuery);
    }

    private function _getSkills(array $body): array
    {
        $escoClient = new EscoSkillsClient();
        $response = $escoClient->post($body);
        return $response['hits']['hits'];
    }

    public function _addSkillsVectors(array $hits, array $vectors): array
    {
        if (count($hits) > 0) {
            foreach ($hits as $hit) {
                $vectors[] = [
                    'script_score' => [
                        'query' => [
                            'match_all' => (object)[]
                        ],
                        'script' => [
                            'source' => 'knn_score',
                            'lang' => 'knn',
                            'params' => [
                                'field' => 'vector_description',
                                'query_value' => $hit['_source']['vector_description'],
                                'space_type' => 'innerproduct'
                            ]
                        ]
                    ]
                ];
            }
        }
        return $vectors;
    }
}
