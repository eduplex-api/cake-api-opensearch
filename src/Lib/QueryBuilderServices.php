<?php
declare(strict_types=1);

namespace OpenSearch\Lib;


use App\Lib\FullBaseUrl;
use Cake\Http\Exception\BadRequestException;

class QueryBuilderServices extends QueryBuilderBase
{
    public function buildQuery(): array
    {
        $limit = $this->_getQueryParams()['limit'] ?? null;
        $page = $this->_getQueryParams()['page'] ?? null;
        $size = $limit ?? 10;
        $from = $page ? $size * ($page - 1) : 0;
        $semanticText = $this->_getQueryParams()['semantic_text'] ?? '';
        $searchQuery = [
            'from' => $from,
            'size' => $size,
            '_source' => [
                'exclude' => ['highlight_vector']
            ],
            'track_scores' => true,
            'query' => [
                'function_score' => [
                    'score_mode' => 'sum',
                    'boost_mode' => 'multiply',
                    'query' => [
                        'bool' => [
                            'must' => $this->getTextQuery()
                        ]
                    ]
                ]
            ]
        ];
        if (!$semanticText) {
            $searchQuery['query']['function_score']['functions'] = $this->_getFunctionsToModifyScores();
        }
        $searchQuery['aggs'] = $this->_getFacetsToReturn();
        $searchQuery['query']['function_score']['query']['bool']['filter'] = $this->_getFilters();
        $searchQuery['sort'] = $this->_getSortParams();
        return $searchQuery;
    }

    public function _buildFallbackQuery(): ?array
    {
        $text = $this->_getQueryParams()['text'] ?? '';
        if ($text) {
            $searchQuery = $this->buildQuery();
            $searchQuery['query']['function_score']['query']['bool']['must'] = [
                [
                    'wildcard' => [
                        'title' => [
                            'value' => '*' . $text . '*',
                            'case_insensitive' => true
                        ]
                    ]
                ]
            ];
            return $searchQuery;
        }
        return null;

    }

    private function _getFacetsToReturn(): array
    {
        $language = $this->_getLanguage();
        $typeFacet = 'i18n_type.' . $language . '.keyword';
        $cityFacet = 'courses.i18n_city.city_name.' . $language .'.keyword';
        return [
            'tags.display_name.keyword' => [
                'terms' => [
                    'field' => 'tags.display_name.keyword',
                    'size' => 50
                ]
            ],
            'tags.grade' => [
                'terms' => [
                    'field' => 'tags.grade',
                    'size' => 10
                ]
            ],
            'trainer_list.fullname.keyword' => [
                'terms' => [
                    'field' => 'trainer_list.fullname.keyword',
                    'size' => 50
                ]
            ],
            'language.keyword' => [
                'terms' => [
                    'field' => 'language.keyword',
                    'size' => 20
                ]
            ],
            'categories.display_name.keyword' => [
                'terms' => [
                    'field' => 'categories.display_name.keyword',
                    'size' => 50
                ]
            ],
            'provider.name.keyword' => [
                'terms' => [
                    'field' => 'provider.name.keyword',
                    'size' => 50
                ]
            ],
            $typeFacet => [
                'terms' => [
                    'field' => $typeFacet,
                    'size' => 50
                ]
            ],
            $cityFacet => [
                'terms' => [
                    'field' => $cityFacet,
                    'size' => 50
                ]
            ]
        ];
    }

    private function _getFilters(): array
    {
        $filters = [];
        if (isset($this->_getQueryParams()['facets'])) {
            $facets = json_decode($this->_getQueryParams()['facets'], true);
            foreach ($facets as $key => $value) {
                $filters[] = [
                    'terms' => [
                        $key => $value
                    ]
                ];
            }
        }
        if (isset($this->_getQueryParams()['tag'])) {
            $tags = explode(',', $this->_getQueryParams()['tag']);
            $filters[] = [
                'terms' => [
                    'tags.tag.keyword' => $tags
                ]
            ];
        }
        if (isset($this->_getQueryParams()['tag_esco_uris'])) {
            $tags = explode(',', $this->_getQueryParams()['tag_esco_uris']);
            $filters[] = [
                'terms' => [
                    'tags.esco_uri.keyword' => $tags
                ]
            ];
        }
        if (isset($this->_getQueryParams()['category'])) {
            $filters[] = [
                'term' => [
                    'categories.category.keyword' => $this->_getQueryParams()['category']
                ]
            ];
        }
        if (isset($this->_getQueryParams()['seller_id'])) {
            $sellerIds = explode(',', $this->_getQueryParams()['seller_id']);
            $filters[] = [
                'terms' => [
                    'provider.id' => $sellerIds
                ]
            ];
        }
        if (isset($this->_getQueryParams()['is_internal'])) {
            $filters[] = [
                'term' => [
                    'is_internal' => (bool)$this->_getQueryParams()['is_internal']
                ]
            ];
        }
        if (isset($this->_getQueryParams()['user_level'])) {
            $userLeveLFilters = [];
            $userLeveLFilters[] = [
                'range' => [
                    'modules_avg_grade' => [
                        'gte' => $this->_getQueryParams()['user_level'] - 8,
                        'lte' => $this->_getQueryParams()['user_level'] + 17
                    ]
                ]
            ];
            $userLeveLFilters[] = [
                'bool' => [
                    'must_not' => [
                        'exists' => [
                            'field' => 'modules_avg_grade'
                        ]
                    ]
                ]
            ];
            $filters[] = [
                'bool' => [
                    'should' => $userLeveLFilters
                ]
            ];
        }
        if (isset($this->_getQueryParams()['has_exclusive_tag']) && !$this->_getQueryParams()['has_exclusive_tag']) {
            $exclusiveFilter = [];
            $exclusiveFilter[] = [
                'term' => [
                    'has_exclusive_tag' => false
                ]
            ];
            $exclusiveFilter[] = [
                'bool' => [
                    'must_not' => [
                        'exists' => [
                            'field' => 'has_exclusive_tag'
                        ]
                    ]
                ]
            ];
            if (isset($this->_getQueryParams()['exclusive_tags'])) {
                $tagsSlug = explode(',', $this->_getQueryParams()['exclusive_tags']);
                $exclusiveFilter[] = [
                    'terms' => [
                        'tags.tag.keyword' => $tagsSlug
                    ]
                ];
            }
            $filters[] = [
                'bool' => [
                    'should' => $exclusiveFilter
                ]
            ];
        }
        return $filters;
    }

    private function _getFunctionsToModifyScores(): array
    {
        return [
            [
                'filter' => [
                    'term' => [
                        'provider_is_verified' => true
                    ]
                ],
                'weight' => 1.2
            ],
            [
                'filter' => [
                    'term' => [
                        'provider_is_topseller' => true
                    ]
                ],
                'weight' => 1.2
            ],
            [
                'filter' => [
                    'term' => [
                        'is_bookable' => true
                    ]
                ],
                'weight' => 7
            ],
            [
                'filter' => [
                    'term' => [
                        'has_boost' => true
                    ]
                ],
                'weight' => 7
            ],
            [
                'field_value_factor' => [
                    'field' => 'rating_rounded',
                    'factor' => 0.01,
                    'missing' => 250
                ]
            ]
        ];
    }

    private function _getSortParams(): array
    {
        $sortParams = [];
        if (isset($this->_getQueryParams()['sort_by'])) {
            switch ($this->_getQueryParams()['sort_by']) {
                case 'title':
                    $sortParams[] = [
                        'title.keyword' => [
                            'order' => 'asc'
                        ]
                    ];
                    break;
                case 'total_price':
                    $order = $this->_getQueryParams()['order'] ?? 'desc';
                    $sortParams[] = [
                        'total_price' => [
                            'order' => $order
                        ]
                    ];
                    break;
                case 'ratings':
                    $sortParams[] = [
                        'rating' => [
                            'order' => 'desc',
                            'unmapped_type' => 'long'
                        ]
                    ];
                    break;
                case 'next_course':
                    $sortParams[] = [
                        'next_course_date' => [
                            'order' => 'asc'
                        ]
                    ];
                    break;
                default:
                    throw new BadRequestException('Invalid sort param: ' . $this->_getQueryParams()['sort_by']);
            }
            $sortParams[] = [
                '_score' => [
                    'order' => 'desc'
                ],
                'has_boost' => [
                    'order' => 'desc'
                ]
            ];
        }
        return $sortParams;
    }

    public function getTextQuery(): array
    {
        $text = $this->_getQueryParams()['text'] ?? '';
        $semanticText = $this->_getQueryParams()['semantic_text'] ?? '';
        $textQuery = [];
        if ($semanticText) {
            $domain = FullBaseUrl::host();
            $vector = $this->_sbertService->vectoriseViaProxy($semanticText, $domain);
            $textQuery[] = [
                'bool' => [
                    'should' => [
                        [
                            'script_score' => [
                                'query' => [
                                    'match_all' => (object)[]
                                ],
                                'script' => [
                                    'source' => 'knn_score',
                                    'lang' => 'knn',
                                    'params' => [
                                        'field' => 'highlight_vector',
                                        'query_value' => $vector,
                                        'space_type' => 'innerproduct'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        } else {
            $textQuery[] = [
                'multi_match' => [
                    'query' => $text,
                    'fields' => [
                        'title^2',
                        'highlight',
                        'tags.display_name',
                        'categories.display_name',
                        'module_titles'
                    ],
                    'fuzziness' => 'AUTO',
                    'zero_terms_query' => 'all'
                ]
            ];
        }
        return $textQuery;
    }
}
