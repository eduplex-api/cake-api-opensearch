<?php

declare(strict_types = 1);

namespace OpenSearch\Lib\Client;

use Cake\Http\Client\Response;
use Cake\Http\Exception\InternalErrorException;
use Cake\Http\Exception\NotImplementedException;

class Client
{
    private \Cake\Http\Client $_cakeHttp;

    public function __construct()
    {
        $this->_cakeHttp = new \Cake\Http\Client();
    }

    public static function getSearchDomain(): string
    {
        $domain = env('OPEN_SEARCH_DOMAIN', 'https://search.eduplex.eu');
        if (!$domain) {
            throw new InternalErrorException('OPEN_SEARCH_DOMAIN is not defined');
        }
        return $domain;
    }

    public function get(string $url, $data = [], array $options = []): Response
    {
        return $this->_cakeHttp->get($url, $data, $options);
    }

    public function post(string $url, $data = [], array $options = []): Response
    {
        return $this->_cakeHttp->post($url, $data, $options);
    }

    public function patch(string $url, $data = [], array $options = []): Response
    {
        throw new NotImplementedException(
            'Patch not allowed in OpenSearch\Lib\Client AccessPolicy in AWS OpenSearch service');
    }

    public function delete(string $url, $data = [], array $options = []): Response
    {
        return $this->_cakeHttp->delete($url, $data, $options);
    }

    public function put(string $url, $data = [], array $options = []): Response
    {
        return $this->_cakeHttp->put($url, $data, $options);
    }
}
