<?php
declare(strict_types=1);

namespace OpenSearch\Lib\Client;

use OpenSearch\Lib\QueryBuilderBase;
use OpenSearch\Lib\QueryBuilderServices;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\ServerRequest;
use SbertService\Lib\SbertService;

class ServiceClient extends OpenSearchBase
{
    public function __construct($tenant)
    {
        if (!$tenant) {
            throw new BadRequestException('Missing tenant param');
        }
        $this->url = env('OPEN_SEARCH_DOMAIN')  . '/' . $tenant;
        parent::__construct();
    }

    protected function _getQueryBuilder(SbertService $sbert, ServerRequest $request): QueryBuilderBase
    {
        return new QueryBuilderServices($sbert, $request);
    }

    protected function _hasFallbackQuery(): bool
    {
        return true;
    }
}
