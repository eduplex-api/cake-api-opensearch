<?php
declare(strict_types=1);

namespace OpenSearch\Lib\Client;

use Cake\Http\Exception\InternalErrorException;
use OpenSearch\Lib\QueryBuilderBase;
use Cake\Http\Client;
use Cake\Http\ServerRequest;
use RestApi\Lib\Exception\DetailedException;
use SbertService\Lib\SbertService;

abstract class OpenSearchBase
{
    protected Client $http;
    protected string $url;

    public function __construct()
    {
        $this->http = new Client();
    }

    protected abstract function _getQueryBuilder(SbertService $sbert, ServerRequest $request): QueryBuilderBase;

    protected function _hasFallbackQuery(): bool
    {
        return false;
    }

    public function sendRequest(SbertService $sbert, ServerRequest $request): array
    {
        $queryBuilder = $this->_getQueryBuilder($sbert, $request);
        $body = $queryBuilder->buildQuery();
        $data = $this->post($body);
        if ($data['hits']['total']['value'] === 0 && $this->_hasFallbackQuery()) {
            $fallbackBody = $queryBuilder->_buildFallbackQuery();
            if ($fallbackBody) {
                $data = $this->post($fallbackBody);
            }
        }
        return $data;
    }

    public function post(array $body): array
    {
        $url = $this->url . '/_search';
        $response = $this->http->post($url, json_encode($body), self::getOptions());
        if ($response->isSuccess()) {
            return $response->getJson();
        } else {
            throw new DetailedException($response->getStringBody());
        }
    }

    public static function getAuthOpenSearch(): string
    {
        $authLrs = env('AUTH_OPEN_SEARCH');
        if (!$authLrs) {
            throw new InternalErrorException('AUTH_OPEN_SEARCH env is missing');
        }
        return 'Basic ' . $authLrs;
    }

    public static function getOptions(): array
    {
        $headers = [
            'Authorization' => self::getAuthOpenSearch(),
            'Content-Type' => 'application/json'
        ];
        return [
            'headers' => $headers
        ];
    }
}
