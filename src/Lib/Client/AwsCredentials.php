<?php

declare(strict_types = 1);

namespace OpenSearch\Lib\Client;

use Aws\Credentials\Credentials;
use Cake\Http\Exception\InternalErrorException;

class AwsCredentials extends Credentials
{
    public function __construct()
    {
        $accessKey = env('ACCESS_KEY_S3_SQS');
        if (!$accessKey) {
            throw new InternalErrorException('Empty env SECRET_S3_SQS');
        }
        $secret = env('SECRET_S3_SQS');
        if (!$secret) {
            throw new InternalErrorException('Empty env ACCESS_KEY_S3_SQS');
        }
        parent::__construct($accessKey, $secret);
    }
}
