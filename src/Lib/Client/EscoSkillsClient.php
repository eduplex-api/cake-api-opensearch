<?php
declare(strict_types=1);

namespace OpenSearch\Lib\Client;

use OpenSearch\Lib\QueryBuilderBase;
use OpenSearch\Lib\QueryBuilderEsco;
use Cake\Http\ServerRequest;
use SbertService\Lib\SbertService;

class EscoSkillsClient extends OpenSearchBase
{
    public function __construct()
    {
        $this->url = env('OPEN_SEARCH_DOMAIN') . '/esco_skills_titan';
        parent::__construct();
    }

    protected function _getQueryBuilder(SbertService $sbert, ServerRequest $request): QueryBuilderBase
    {
        return new QueryBuilderEsco($sbert, $request);
    }
}
