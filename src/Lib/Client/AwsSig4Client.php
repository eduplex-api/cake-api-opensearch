<?php

namespace App\OpenSearch\Lib\Client;

use Aws\Signature\SignatureV4;
use Aws\Sts\StsClient;
use Cake\Http\Client\Response;
use Cake\Http\Exception\InternalErrorException;
use GuzzleHttp\Exception\RequestException;
use OpenSearch\Lib\Client\AwsCredentials;
use OpenSearch\Lib\Client\Client;
use Psr\Http\Message\ResponseInterface;

class AwsSig4Client
{
    public function doRequest(string $method, string $url, $body): Response
    {
        if (is_string($body)) {
            $payload = $body;
        } else {
            $payload = json_encode($body);
        }

        $client = new \GuzzleHttp\Client();

        $host = $this->getHost();
        if (str_starts_with($url, "https://$host")) {
            $endpoint = $url;
        } else {
            throw new InternalErrorException("Url $url in OpenSearch client should start with $host");
        }

        $awsCredentials = new AwsCredentials();
        $stsClient = new StsClient([
            'region' => $this->getRegion(),
            'version' => 'latest',
            'http'        => ['connect_timeout' => 5], // Aumenta timeout a 5s
            'credentials' => [
                'key'    => $awsCredentials->getAccessKeyId(),
                'secret' => $awsCredentials->getSecretKey(),
            ]
        ]);
        debug($awsCredentials->getAccessKeyId());
        $result = $stsClient->assumeRole([
            'RoleArn'         => 'arn:aws:iam::439042530115:role/OpenSearchAccessRole-OpenSearchService2',
            'RoleSessionName' => 'OpenSearchSession', // Nombre de la sesión
        ]);
        $credentials = $result->get('Credentials');
        debug($credentials);

        $sigV4 = new SignatureV4($this->getService(), $this->getRegion());

        $request = new \GuzzleHttp\Psr7\Request($method, $endpoint, [
            'Content-Type' => 'application/json',
            'Host' => $host
        ], $payload);

        $signedRequest = $sigV4->signRequest($request, new AwsCredentials());

        try {
            $response = $client->send($signedRequest);
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $bodyString = $response->getBody()->getContents();
                return $this->_buildResponse($e->getResponse(), $bodyString);
            } else {
                throw $e;
            }
        }

        return $this->_buildResponse($response, (string)$response->getBody());
    }

    private function _buildResponse(ResponseInterface $response, string $resBody)
    {
        $headers = [];
        foreach ($response->getHeaders() as $header) {
            $headers[] = $header[0];
        }
        $res = new Response($headers, $resBody);
        return $res->withStatus($response->getStatusCode());
    }

    private function getHost(): string
    {
        $exploded = explode('/', Client::getSearchDomain());
        return array_pop($exploded);
    }

    public function getRegion()
    {
        return 'eu-central-1';//TODO
    }

    public function getService()
    {
        return 'es';//TODO
    }
}
