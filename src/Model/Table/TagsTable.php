<?php
declare(strict_types=1);

namespace OpenSearch\Model\Table;

use Cake\ORM\Behavior\TimestampBehavior;
use RestApi\Model\Table\RestApiTable;

class TagsTable extends RestApiTable
{
    protected function getTablePrefix(): string
    {
        return '';
    }

    public function initialize(array $config): void
    {
        $this->addBehavior(TimestampBehavior::class);
    }

    public function getAllUrisForSearchFilter(bool $onlyIndexed = true): array
    {
        $q = $this->find()
            ->where([
                'esco_uri is not null',
                'esco_uri != ""'
            ]);
        if ($onlyIndexed) {
            $q->where(['is_indexed = 1']);
        }
        $elems = $q->all();
        $arrayToRet = [];
        foreach ($elems as $elem) {
            $arrayToRet[] = $elem->esco_uri;
        }
        return $arrayToRet;
    }

    public function getTagsByEscoUris($escoUris): array
    {
        return $this->find()
            ->where(['esco_uri in' => $escoUris])
            ->toArray();
    }
}
